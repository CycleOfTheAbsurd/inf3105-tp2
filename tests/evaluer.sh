#!/bin/bash
##########################################################################
# UQAM - Département d'informatique
# INF3105 - Structures de données et algorithmes
# Automne 2019
# TP2
# http://ericbeaudry.uqam.ca/INF3105/tp1/
# beaudry.eric@uqam.ca
#
# Script d'évaluation
#
# Instructions:
# 1. Déposer ce script avec les fichiers problèmes dans un répertoire 
#    distinct (ex: tests).
# 2. Compiler le valideur: g++ -o valideur valideur.cpp -O
# 3. Se placer dans le répertoire contenant votre programme ou contenant
#    la liste des soumissions Oto (*.tp_oto).
# 4. Lancer ce script (ex: ../tests/evaluer.sh).
##########################################################################

# Obtenir le chemin du répertoire contenant le présent script et les fichiers tests
pushd `dirname $0`
repertoire_tests=`pwd`
tests=`ls test_[A-Z]??.txt`
popd

echo "UQAM | Département d'informatique"
echo "INF3105 | Structures de données et algorithmes"
echo "Évaluation du TP2"
echo

if [ `pwd` -ef $repertoire_tests ];
then
    echo "Ce script doit être dans un répertoire différent de celui contenant votre tp2."
    echo "Ce script a été arrêté afin de ne pas écraser les fichiers test_[CDFG]+.txt !"
    exit -2;
fi

########### Détection du valideur de résultats #######
# Exécutable du programme de validation
valideur="${repertoire_tests}/valideur"
if [ -x "${valideur}-`uname`-`uname -p`" ]
then
    valideur="${valideur}-`uname`-`uname -p`"
else
    if [ -x "${valideur}-`uname`" ]
    then
        valideur="${valideur}-`uname`"
    fi
fi
######################################################


# Détection de la disponibilité de l'utilitaire time sous Linux (peut afficher 0.00)
echo "Détection de /usr/bin/time..."
/usr/bin/time -f %U echo 2>&1 > /dev/null
souslinux=$?

# Mettre une limite de temps.
ulimit -t 180 -v 524288 -f 512
echo "Limites :"
ulimit -t -v -f

# Détection du CPU
if [ -e /proc/cpuinfo ] ; then
    cpuinfo=`grep "model name" /proc/cpuinfo | sort -u | cut -d ":" -f 2`
else
    cpuinfo="?"
fi

function Nettoyer
{
    echo "Nettoyage"
    # Au cas où le Makefile des étudiants ne ferait pas un «make clean» correctement.
    #make clean

    rm -f *.o* *.gch tp[1-3] *-req?+.txt
    # Au besoin, nettoyer les précédents fichiers logs
    rm -f log*.txt
}


## La fonction qui évalue un TP.
function EvaluerTP
{
    # Au cas où le Makefile des étudiants ne ferait pas un «make clean» correctement.
    #rm -f *.o* *.gch tp2 test*+.txt
    # Au besoin, nettoyer les fichiers logs
    #rm -f log*.txt
    
    logfile="log-${date}.txt"

    # Extraction des codes permanents dans les fichiers sources
    echo "Rapport de correction de : " > ${logfile}
    ((pwd | grep -oh [a-zA-Z]{4}[0-9]{8}) && grep -oh  [a-zA-Z]{4}[0-9]{8} *.{h*,c*} ) | sort -u >> ${logfile}
    echo -e "\n\n" >> ${logfile}
    
    # Pour statistiques : nombre de lignes de code...
    echo "Taille des fichiers source :" >> ${logfile}
    wc *.{c*,h*}  >> ${logfile}
    

    taille=`wc *.{c*,h*} | grep total`
    checkcopie=`cat *.h | grep -o "H__" | wc -l`
    checkSTL=`cat *.h | grep -o -E "<set>|<map>|<vector>|<list>" | wc -l`
    sommaire="${sommaire}$checkcopie\t$checkSTL\t$taille\t|\t"

    echo -e "\nCompilation ..."
    echo -e "\nCompilation ..." >> ${logfile}

    if [ ! -e Makefile ]; then
        echo "ERREUR : Makefile inexistant!"
        echo "  ERREUR : Makefile inexistant!" >> ${logfile}
        return;
    fi

    #make clean
    #make 2>&1 >> ${logfile}
    echo -ne 

    if [ ! -x tp2 ]; then
        echo "ERREUR : l'executable tp2 na pas été produit!"
        echo "  ERREUR : l'executable tp2 na pas été produit!" >> ${logfile}
        return
    fi

    echo -e "\nVérification du fonctionnement et Évaluation des performances..."
    echo -e "\nVérification du fonctionnement et Évaluation des performances..." >> ${logfile}
	
    
    
    
    echo "Machine : " `hostname` "."
    echo "#Machine : " `hostname` "."  > $logfile
    echo "CPU : $cpuinfo"
    echo "CPU : $cpuinfo"  > $logfile
    echo "Date début : $date."
    echo "#Date début : $date."  >> $logfile
    echo "Limite de `ulimit -t` secondes par test."
    echo "#Limite de `ulimit -t` secondes par test."  >> $logfile
    echo 
    echo "#" >> $logfile

    echo -e "Fichier_test\tNbBons\tTotal\tMém.(k)"
    echo -e "Fichier_test\tNbBons\tTotal\tMém.(k)" >> $logfile



    for test in $tests;
    do
        fichier="${repertoire_tests}/${test}"

        if [ $souslinux -eq 0 ]; then
            tG="`(/usr/bin/time -f "%U\t%Mk\t" ./tp2 < $fichier > ${test%.txt}+.txt) 2>&1 | tail -n 1`"
        else
            tG="`(time -p ./tp2 < $fichier > ${test%.txt}+.txt) 2>&1 | egrep user | cut -f 2 -d " "`\t---"
        fi

	 
        if ( [ -x ${valideur} ] && [ -e "${test%.txt}+.txt" ] )
        then
            verif=`$valideur -q $fichier "${fichier%.txt}+.txt" ${test%.txt}+.txt`
        else
            verif="?"
        fi

        echo -e "$test\t$verif\t$tG"
        echo -e "$test\t$verif\t$tG" >> $logfile

        sommaire="${sommaire}$verif\t$tG"
    done

    #make clean
}

# Lister les soumissions Oto (répertoires terminant par .tp_oto)
tps=`ls -1 | grep .tp_oto`
# Si aucun répertoire .tp_oto n'existe, essayer le répertoire courant (auto-évaluation)
if [ ! -n "$tps" ]; then
    tps="."
fi

# Génération de l'entête du rapport
date=`date +%Y%m%d_%H%M%S`
echo "#Rapport de correction INF3105 / TP1" > "rapport-${date}.txt"
echo -e "#Date:\t${date}" >> "rapport-${date}.txt"
echo -e "#Machine :\t" `hostname` >> "rapport-${date}.txt"
echo -e "#CPU :\t$cpuinfo" >> "rapport-${date}.txt"
echo >> "rapport-${date}.txt"

# Génération des titres des colonnes
echo -n -e "\t\t\t\t\t" >> "rapport-${date}.txt"
for test in $tests;
do
   echo -n -e "$test\t\t\t" >> "rapport-${date}.txt"
done
echo >> "rapport-${date}.txt"

echo -n -e "Soumission\tTraces2013E\tSTL\tTaille\t|\t" >> "rapport-${date}.txt"
for test in $tests;
do
    echo -n -e "NbBons\tCPU\tMem(k)\t" >> "rapport-${date}.txt"
done
echo >> "rapport-${date}.txt"

# Itération sur chaque TP
for tp in $tps; do
    sommaire=""
    echo "## Évaluation du répertoire : $tp"
    pushd $tp
    	EvaluerTP
#       Nettoyer
    popd
    #echo -e ">> ${sommaire}"
    echo -e "${tp}\t${sommaire}" >> "rapport-${date}.txt"
done

