/*
  Valideur pour INF3105 / 2019A / TP2
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <cstring>
#include <algorithm>
#include <string> 
#include <map>

using namespace std;

string enleverEspaces(const string& chaine){
    string resultat = chaine;
    int j=0;
    for(unsigned int i=0;i<chaine.size();i++){
        if(chaine[i]!=' ' && chaine[i]!='\t')
            resultat[j++] = chaine[i];
    }
    resultat[j++] = 0;
    return resultat;
}

int main(int argc, const char** argv)
{
    string nomfichier_test, nomfichier_resultat1, nomfichier_resultat2;
    bool silence=false;
    bool corrige=false;
    for(int i=1,j=0;i<argc;i++){
        if(strcmp(argv[i],"-q")==0 || strcmp(argv[i],"-quiet")==0 || strcmp(argv[i],"-silence")==0)
            silence=true;
        else if(strcmp(argv[i],"-c")==0 || strcmp(argv[i],"-corrige")==0)
            corrige=true;
        else
        switch(j++){
            case 0:
                nomfichier_test = argv[i];
                break;
            case 1:
                nomfichier_resultat1 = argv[i];
                break;
            case 2:
                nomfichier_resultat2 = argv[i];
                break;
        }
    }
    if(nomfichier_resultat2==""){
        cerr << "Valideur pour INF3105/2019A/TP2" << endl;
        cerr << "Syntaxe : valideur text.txt solution+.txt votreresultat+.txt" << endl;
        return 1;
    }
    
    ifstream ifs_test(nomfichier_test.c_str());
    ifstream ifs_solution(nomfichier_resultat1.c_str());
    ifstream ifs_resultat(nomfichier_resultat2.c_str());
    
    if(ifs_test.fail()){
        cerr << "Erreur ouverture test " << nomfichier_test << endl;
        return 1;
    }
    if(ifs_solution.fail()){
        cerr << "Erreur ouverture sol " << nomfichier_resultat1 << endl;
        return 1;
    }
    if(ifs_resultat.fail()){
        cerr << "Erreur ouverture res " << nomfichier_resultat2 << endl;
        return 1;
    }
    
    int nbtests = 0;
    int nbreussi = 0;
    
    int ligne=0;
    while(ifs_test){

        ligne++;
        string lignetest, lignesolution, ligneresultat;
        getline(ifs_test,lignetest);
        getline(ifs_solution, lignesolution);
        getline(ifs_resultat, ligneresultat);
        
        stringstream ss(lignetest);
        string commande;
        ss >> commande;
        
        if(commande.empty()) break;
        if(lignesolution.empty()) break;
        
        std::transform(lignesolution.begin(), lignesolution.end(), lignesolution.begin(), ::tolower);
        std::transform(ligneresultat.begin(), ligneresultat.end(), ligneresultat.begin(), ::tolower);

        if(enleverEspaces(lignesolution)==enleverEspaces(ligneresultat))
            nbreussi++;
        else if(!silence){
            cerr << "Ligne #" << ligne << " (" << commande << ") : ÉCHEC" << endl;
            cerr << " V1:" << lignesolution << endl;
            cerr << " V2:" << ligneresultat << endl;
        }

        nbtests++;
        
    }
    
    cout << nbreussi;
    if(!corrige) cout << '/' << nbtests;

    cout << endl;

    return 0;
}

