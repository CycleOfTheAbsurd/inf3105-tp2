/* UQAM / Département d'informatique
   INF3105 - Structures de données et algorithmes
   Squelette pour classe générique ArbreMap<K,V> pour le Lab8 et le TP2.

   AUTEUR(S):
	1) Nom + Code permanent du l'étudiant.e 1
	2) Nom + Code permanent du l'étudiant.e 2
*/

#if !defined(__ARBREMAP_H__)
#define __ARBREMAP_H__

#include "arbreavl.h"

template <class K, class V>
class ArbreMap {
  private:
	class Entree{
		public:
			Entree(const K& c) :cle(c),valeur() {}
			Entree(const K& c, const V&v) :cle(c),valeur(v) {}
			K cle;
			V valeur;
			bool operator < (const Entree& e) const { return cle < e.cle; }
			bool operator > (const Entree& e) const { return e.cle < cle; }
			bool operator == (const Entree& e) const { return cle == e.cle; }
			bool operator != (const Entree& e) const { return cle != e.cle; }
	};

	ArbreAVL<Entree> entrees;

  public:
	bool contient(const K&) const;

	// Si l'élément existe déjà, il est remplacé
	void inserer(const K&, const V&);
	void enlever(const K&);
	void vider();
	bool vide() const;
	~ArbreMap() {
		vider();
	}

	const V& operator[] (const K&) const;
	V& operator[] (const K&);

	class Iterateur {
		public:
			Iterateur(const ArbreMap& a) : iter(a.entrees.debut()) {}
			Iterateur(typename ArbreAVL<Entree>::Iterateur i) : iter(i) {}
			operator bool() const;
			bool operator!() const;

			Iterateur& operator++();
			Iterateur operator++(int);

			const K& cle() const;
			V& valeur() const;

		private:
			typename ArbreAVL<Entree>::Iterateur iter;
	};

	Iterateur debut() const;
	Iterateur fin() const;
	Iterateur rechercher(const K& cle) const;
	Iterateur rechercherEgalOuSuivant(const K& cle) const;
	Iterateur rechercherEgalOuPrecedent(const K& cle) const;
};


template <class K, class V>
void ArbreMap<K,V>::vider() {
	entrees.vider();
}

template <class K, class V>
bool ArbreMap<K,V>::vide() const {
	return entrees.vide();
}

template <class K, class V>
void ArbreMap<K,V>::inserer(const K& k, const V& v) {
	entrees.inserer(Entree(k, v));
}

template <class K, class V>
void ArbreMap<K,V>::enlever(const K& c) {
	entrees.enlever(c);
}

template <class K, class V>
bool ArbreMap<K,V>::contient(const K& c) const {
	return entrees.contient(c);
}

// Pour la lecture seule, on doit s'assurer que la clé existe
template <class K, class V>
const V& ArbreMap<K,V>::operator[] (const K& c) const {
	assert(entrees.contient(c) && "Cette clé ne se trouve pas dans l'arbre");
	Iterateur it = rechercher(c);
	return it.valeur();
}

// Pour l'écriture, un Noeud avec la valeur par défaut est créé si la clé n'existe pas
template <class K, class V>
V& ArbreMap<K,V>::operator[] (const K& c) {
	if (!entrees.contient(c)) {
		entrees.inserer(Entree(c));
	}
	Iterateur it =  rechercher(c);
	return it.valeur();
}

// Retourne Itérateur
template <class K, class V>
typename ArbreMap<K,V>::Iterateur ArbreMap<K,V>::debut() const {
	return Iterateur(*this);
}

template <class K, class V>
typename ArbreMap<K,V>::Iterateur ArbreMap<K,V>::fin() const {
	return Iterateur(entrees.fin());
}

template <class K, class V>
typename ArbreMap<K,V>::Iterateur ArbreMap<K,V>::rechercher(const K& cle) const {
	return Iterateur(entrees.rechercher(Entree(cle)));
}

template <class K, class V>
typename ArbreMap<K,V>::Iterateur ArbreMap<K,V>::rechercherEgalOuSuivant(const K& cle) const {
	return Iterateur(entrees.rechercherEgalOuSuivant(Entree(cle)));
}

template <class K, class V>
typename ArbreMap<K,V>::Iterateur ArbreMap<K,V>::rechercherEgalOuPrecedent(const K& cle) const {
	return new Iterateur(entrees.rechercherEgalOuPrecedent(new Entree(cle)));
}

// Itérateur
template <class K, class V>
ArbreMap<K,V>::Iterateur::operator bool() const {
	return iter;
}

template <class K, class V>
bool ArbreMap<K,V>::Iterateur::operator!() const {
	return !iter;
}

//Pre-inrement
template <class K, class V>
typename ArbreMap<K,V>::Iterateur& ArbreMap<K,V>::Iterateur::operator++() {
	this->iter++;
	return *this;
}

//Post-increment
template <class K, class V>
typename ArbreMap<K,V>::Iterateur ArbreMap<K,V>::Iterateur::operator++(int) {
	Iterateur copie(this->iter);
	operator++();
	return copie;
}

template <class K, class V>
const K& ArbreMap<K,V>::Iterateur::cle() const {
	return (*iter).cle;
}

template <class K, class V>
V& ArbreMap<K,V>::Iterateur::valeur() const {
	return (*iter).valeur;
}

#endif
