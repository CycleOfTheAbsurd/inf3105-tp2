/*
  INF3105 - Structures de données et algorithmes
  UQAM / Département d'informatique
  Automne 2019

  Squelette de départ. Vous pouvez modifier ce fichier à votre guise.
  Vous n'êtes pas oubligés de l'utiliser.
*/

#include "pile.h"
#include "inventaire.h"

ListeIngredients& ListeIngredients::operator+=(const ListeIngredients& autre) {
	ArbreMap<std::string,int>::Iterateur iter = autre.ingredients.debut();
	while (iter) {
		ingredients[iter.cle()] += iter.valeur();
		iter++;
	}
	return *this;
}

ListeIngredients& ListeIngredients::operator*=(int facteur) {
	ArbreMap<std::string,int>::Iterateur iter = ingredients.debut();
	while (iter) {
		ingredients[iter.cle()] *= facteur;
		iter++;
	}
	return *this;
}

ListeIngredients ListeIngredients::operator+(const ListeIngredients& autre) const {
	ListeIngredients nouvelle = *this;
	ArbreMap<std::string,int>::Iterateur iter = autre.ingredients.debut();
	while (iter) {
		nouvelle.ingredients[iter.cle()] += iter.valeur();
		iter++;
	}
	return nouvelle;
}

ListeIngredients ListeIngredients::operator*(int facteur) const {
	ListeIngredients nouvelle = *this;
	ArbreMap<std::string,int>::Iterateur iter = nouvelle.ingredients.debut();
	while (iter) {
		nouvelle.ingredients[iter.cle()] *= facteur;
		iter++;
	}
	return nouvelle;
}

ListeIngredients& ListeIngredients::operator=(const ListeIngredients& autre) {
	ingredients.vider();
	ArbreMap<std::string, int>::Iterateur iter = autre.ingredients.debut();
	while(iter) {
		ingredients[iter.cle()] = iter.valeur();
		iter++;
	}
	return *this;
}

void ListeIngredients::ajouter(const std::string ingredient, const int quantite) {
	ingredients.inserer(ingredient, quantite);
}

typename ArbreMap<std::string,int>::Iterateur ListeIngredients::listeIngredients() const {
	return ingredients.debut();
}

std::istream& operator >> (std::istream& is, ListeIngredients& liste) {
	// Vider liste.
	liste.ingredients.vider();

	std::string chaine;
	is >> chaine;
	while(is && chaine!="---"){
		int quantite;
		is >> quantite;
		liste.ajouter(chaine, quantite);
		is >> chaine;
	}
	return is;
}

// Inventaire
Inventaire& Inventaire::operator+=(const Inventaire& autre) {

	ArbreMap<std::string, ArbreMap<Date, int>>::Iterateur iter_ingredients = autre.stock.debut();
	while (iter_ingredients) {
		ArbreMap<Date, int>::Iterateur iter_dates = iter_ingredients.valeur().debut();
		while (iter_dates) {
			ajouterIngredient(iter_ingredients.cle(), iter_dates.valeur(), iter_dates.cle());
			iter_dates++;
		}
		iter_ingredients++;
	}
	return *this;
}

// Toujours vérifier aIngredients avant cette opération
Inventaire& Inventaire::operator-=(const ListeIngredients& liste) {
	ArbreMap<std::string, int>::Iterateur iter = liste.listeIngredients();
	while(iter) {
		retirerIngredient(iter.cle(), iter.valeur());
		iter++;
	}
	return *this;
}

bool Inventaire::aIngredients(const Date& date, const ListeIngredients& recette) {
	bool suffisant = true;
	ArbreMap<std::string, int>::Iterateur iter = recette.listeIngredients();
	while(iter && suffisant) {
		suffisant = iter.valeur() <= quantiteIngredient(date, iter.cle());
		iter++;
	}
	return suffisant;
}

int Inventaire::quantiteIngredient(const Date& date, const std::string& nom_ingredient) {
	int quantite = 0;
	if (stock.contient(nom_ingredient)) {
		ArbreMap<Date, int>::Iterateur iter = stock[nom_ingredient].debut();
		// Noeuds qui sont expirés. Ne peut pas les enlever au fur et à mesure, car ça risque de dérègler l'itérateur
		Pile<Date> aRetirer = Pile<Date>();
		while (iter) {
			if(date < iter.cle()) {
				quantite += iter.valeur();
			} else {
				aRetirer.empiler(iter.cle());
			}
			iter++;
		}
		while (!aRetirer.vide()) {
			stock[nom_ingredient].enlever(aRetirer.depiler());
		}
	}
	return quantite;
}

void Inventaire::retirerIngredient(const std::string& nom_ingredient, int quantite) {
	ArbreMap<Date, int>::Iterateur iter = stock[nom_ingredient].debut();
	// Noeuds qui sont vides après le retrait. Ne peut pas les enlever au fur et à mesure, car ça dérègle l'itérateur
	Pile<Date> aRetirer = Pile<Date>();
	while (iter && quantite > 0) {
		if(iter.valeur() >= quantite) {
			int q = quantite;
			quantite -= iter.valeur();
			iter.valeur() -= q;
		} else {
			quantite -= iter.valeur();
			iter.valeur() = 0;
			aRetirer.empiler(iter.cle());
		}
		iter++;
	}
	while (!aRetirer.vide()) {
		stock[nom_ingredient].enlever(aRetirer.depiler());
	}
}

void Inventaire::ajouterIngredient(const std::string& nom_ingredient, const int& quantite, const Date& date) {
	if (!(stock.contient(nom_ingredient) && stock[nom_ingredient].contient(date))) {
		stock[nom_ingredient][date] = 0;
	}
	stock[nom_ingredient][date] += quantite;
}

std::istream& operator >> (std::istream& is, Inventaire& inventaire) {
	// Vider inventaire
	inventaire.stock.vider();
	std::string chaine;
	is >> chaine;
	while(is && chaine!="---"){
		int quantite;
		Date expiration;
		is >> quantite >> expiration;
		inventaire.stock[chaine][expiration] = quantite;
		is >> chaine;
	}
	return is;
}
