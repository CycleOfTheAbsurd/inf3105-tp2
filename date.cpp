/*
  INF3105 - Structures de données et algorithmes
  UQAM / Département d'informatique
  Automne 2019

  Squelette de départ. Vous pouvez modifier ce fichier à votre guise.
  Vous n'êtes pas oubligés de l'utiliser.
*/

#include "date.h"
#include <cstdio>
#include <cassert>

Date::Date()
:jour(), heure(), minute() {}

Date::Date(const int j=0, const int h=0, const int m=0)
:jour(j), heure(h), minute(m) {}

bool Date::operator <(const Date& d) const{
	if (jour < d.jour) {
		return true;
	} else if (jour == d.jour && heure < d.heure) {
		return true;
	} else if (jour == d.jour && heure == d.heure && minute < d.minute ) {
		return true;
	} else {
		return false;
	}
}

bool Date::operator <=(const Date& d) const{
	return *this == d || *this < d;
}

bool Date::operator ==(const Date& d) const{
	return jour == d.jour && heure == d.heure && minute == d.minute;
}

bool Date::operator !=(const Date& d) const{
	return !operator==(d);
}

std::ostream& operator << (std::ostream& os, const Date& d){
	char chaine[40];
	sprintf(chaine, "%dj_%02dh%02dm", d.jour, d.heure, d.minute);
	os << chaine;
	return os;
}

std::istream& operator >> (std::istream& is, Date& d){
	int jours, heures, minutes;
	char j, m, h, underscore;
	is >> jours >> j >> underscore >> heures >> h >> minutes >> m;
	assert(j=='j');
	assert(underscore=='_');
	assert(h=='h' && m=='m');

	d.jour = jours;
	d.heure = heures;
	d.minute = minutes;
	return is;
}

std::ostream& operator << (std::ostream& os, const Intervalle& i){
  os << i.debut  << i.fin;
  return os;
}

std::istream& operator >> (std::istream& is, Intervalle& i){
  char crochetgauche, crochetdroit, virgule;
  is >> crochetgauche >> i.debut >> virgule >> i.fin >> crochetdroit;
  assert(crochetgauche=='[' && virgule==',' && crochetdroit==']');
  return is;
}
