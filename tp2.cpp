/*
INF3105 - Structures de données et algorithmes
UQAM / Département d'informatique
Automne 2019

Squelette de départ. Vous pouvez modifier ce fichier à votre guise.
Vous n'êtes pas oubligés de l'utiliser.
*/

#include <fstream>
#include "inventaire.h"

int tp2(std::istream& entree){
	// - Déclaration du dictionnaire de recettes.
	ArbreMap<std::string, ListeIngredients> recettes = ArbreMap<std::string, ListeIngredients>();

	Inventaire inventaire = Inventaire();
	int nocommande = 0;
	Date date;
	// Compteur d'opérations totales, car la première peut être faite au temps 0, mais pas les autres.
	int nooperations = 0;

	while(entree) {
		 std::string commande;
		 entree >> commande;
		 if(!entree) break;

		 if(commande=="recette") {
			 std::string nomrecette;
			 entree >> nomrecette;
			 ListeIngredients ingredients;
			 entree >> ingredients;
			 recettes.inserer(nomrecette, ingredients);
		 } else if(commande=="reception") {
			 Date date_reception;
			 entree >> date_reception;
			 if(date_reception <= date && nooperations != 0)
				 std::cout << "Attention : ce programme supporte uniquement un ordre chronologique (voir section 3.6 / hypothèse 3)!" << std::endl;
			 date = date_reception;
			 Inventaire inventairerecu;
			 entree >> inventairerecu;
			 inventaire += inventairerecu;
			 nooperations++;
		 } else if(commande=="reservation") {
			 Date date_preparation;
			 entree >> date_preparation;
			 if(date_preparation <= date && nooperations != 0)
				 std::cout << "Attention : ce programme supporte uniquement un ordre chronologique (voir section 3.6 / hypothèse 3)!" << std::endl;
			 date = date_preparation;

			 ListeIngredients ingredients_reservation = ListeIngredients();
			 std::string nomrecette;
			 entree >> nomrecette;
			 while(entree && nomrecette!="---"){
				 int quantite=0;
				 entree >> quantite;
				 ingredients_reservation += (recettes[nomrecette] * quantite);
				 entree >> nomrecette;
			 }
			 std::cout << nocommande << " : ";
			 if (inventaire.aIngredients(date_preparation, ingredients_reservation)) {
			 	std::cout << "OK" << std::endl;
				inventaire -= ingredients_reservation;
			 } else {
			 	std::cout << "Echec" << std::endl;
			 }
			 nocommande++;
			 nooperations++;
		 } else {
			 std::cout << "Commande '" << commande << "' inconnue!" << std::endl;
			 return 2;
		 }
	}

	return 0;
}

int main(int argc, const char** argv) {
	// Gestion de l'entrée :
	//  - lecture dans un fichier si un nom est spécifié.
	//  - sinon, lecture dans std::cin
	if(argc>1) {
		 std::ifstream entree_fichier(argv[1]);
		 if(entree_fichier.fail()) {
			 std::cerr << "Erreur d'ouverture du fichier '" << argv[1] << "'" << std::endl;
			 return 1;
		 }
		 return tp2(entree_fichier);
	} else {
		 return tp2(std::cin);
	}
}
