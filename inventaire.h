/*
  INF3105 - Structures de données et algorithmes
  UQAM / Département d'informatique
  Automne 2019

  Squelette de départ. Vous pouvez modifier ce fichier à votre guise.
  Vous n'êtes pas oubligés de l'utiliser.
*/

#if !defined(_INVENTAIRE__H_)
#define _INVENTAIRE__H_

#include <iostream>
#include "date.h"
#include "arbremap.h"

// Recette
class ListeIngredients {
  public:
	ListeIngredients& operator+=(const ListeIngredients& liste);
	ListeIngredients& operator*=(int quantite);
	ListeIngredients operator+(const ListeIngredients& liste) const;
	ListeIngredients operator*(int quantite) const;
	ListeIngredients& operator=(const ListeIngredients& liste);
	void ajouter(const std::string ingredient, const int quantite);
	ArbreMap<std::string,int>::Iterateur listeIngredients() const;

  private:
	ArbreMap<std::string, int> ingredients;

  friend std::istream& operator >> (std::istream&, ListeIngredients&);
  friend class Inventaire;
};

class Inventaire{
  public:

	Inventaire& operator+=(const Inventaire&);
	Inventaire& operator-=(const ListeIngredients&);
	// Retourne vrai si l'inventaire contient les ingrédients nécessaires pour faire la recette
	bool aIngredients(const Date& date, const ListeIngredients&);

  private:
	// Retourne la quantité non-expirée à Date de l'ingrédient
	int quantiteIngredient(const Date& date, const std::string& nom_ingredient);
	void retirerIngredient(const std::string& nom_ingredient, int quantite);
	void ajouterIngredient(const std::string& nom_ingredient, const int& quantite, const Date& date);
	ArbreMap<std::string, ArbreMap<Date, int>> stock;

  friend std::istream& operator >> (std::istream&, Inventaire&);
};

#endif
