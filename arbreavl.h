/* UQAM / Département d'informatique
   INF3105 - Structures de données et algorithmes
   Squelette pour classe générique ArbreAVL<T> pour le Lab6 et le TP2.
*/

#if !defined(__ARBREAVL_H__)
#define __ARBREAVL_H__
#include <cassert>
#include <cmath>
#include "pile.h"

template <class T>
class ArbreAVL {
  public:
	ArbreAVL();
	ArbreAVL(const ArbreAVL&);
	~ArbreAVL();

	// Annonce l'existance d'une classe Iterateur.
	class Iterateur;

	// Si l'élément existe déjà, il est remplacé
	void inserer(const T&);
	bool contient(const T&) const;
	bool vide() const;
	void vider();
	void enlever(const T&);
	int  hauteur() ;

	// Fonctions pour obtenir un itérateur (position dans l'arbre)
	Iterateur debut() const;
	Iterateur fin() const;
	Iterateur rechercher(const T&) const;
	Iterateur rechercherEgalOuSuivant(const T&) const;
	Iterateur rechercherEgalOuPrecedent(const T&) const;

	// Accès aux éléments de l'arbre via un intérateur.
	const T& operator[](const Iterateur&) const;
	T& operator[](const Iterateur&);

	// Copie d'un arbre AVL.
	ArbreAVL& operator = (const ArbreAVL&);

  private:
	struct Noeud{
		Noeud(const T&);
		T contenu;
		int equilibre;
		Noeud *gauche,
			  *droite;
	};
	Noeud* racine;

	// Fonctions internes
	bool inserer(Noeud*&, const T&);
	void rotationGaucheDroite(Noeud*&);
	void rotationDroiteGauche(Noeud*&);
	void vider(Noeud*&);
	void copier(const Noeud*, Noeud*&) const;
	const T& max(Noeud*) const;
	// Retourne vrai si la hauteur du sous-arbre a changé
	bool enlever(Noeud*&, const T& e);
	int  hauteur(Noeud*&) const;

  public:
	class Iterateur{
	  public:
		Iterateur(const ArbreAVL& a);
		Iterateur(const Iterateur& a);
		Iterateur(const ArbreAVL& a, Noeud* c);

		operator bool() const;
		bool operator!() const;
		bool operator==(const Iterateur&) const;
		bool operator!=(const Iterateur&) const;

		T& operator*() const;

		Iterateur& operator++();
		Iterateur operator++(int);
		Iterateur& operator = (const Iterateur&);
	  private:
		void rechercher(const T&);
		void rechercherEgalOuSuivant(const T&);
		void rechercherEgalOuPrecedent(const T&);

		const ArbreAVL& arbre_associe;
		Noeud* courant;
		Pile<Noeud*> chemin;

	  friend class ArbreAVL;
	};
};


//-----------------------------------------------------------------------------
// ArbreAVL

template <class T>
ArbreAVL<T>::Noeud::Noeud(const T& c)
 : contenu(c), equilibre(0), gauche(NULL), droite(NULL) {
}

template <class T>
ArbreAVL<T>::ArbreAVL()
 : racine(NULL) {
}

template <class T>
ArbreAVL<T>::ArbreAVL(const ArbreAVL<T>& autre)
 : racine(NULL) {
	copier(autre.racine, racine);
}

template <class T>
ArbreAVL<T>::~ArbreAVL() {
	vider(racine);
}

template <class T>
bool ArbreAVL<T>::contient(const T& element) const {
	return rechercher(element);
}

template <class T>
void ArbreAVL<T>::inserer(const T& element) {
	inserer(racine, element);
}

template <class T>
bool ArbreAVL<T>::inserer(Noeud*& noeud, const T& element) {
	if(noeud==NULL) {
		noeud = new Noeud(element);
		return true;
	}
	if(element < noeud->contenu){
		if(inserer(noeud->gauche, element)) {
			noeud->equilibre++;
			if(noeud->equilibre == 0)
				return false;
			if(noeud->equilibre == 1)
				return true;
			assert(noeud->equilibre==2);
			if(noeud->gauche->equilibre == -1)
				rotationDroiteGauche(noeud->gauche);
			rotationGaucheDroite(noeud);
		}
		return false;
	}
	else if(noeud->contenu < element){
		if(inserer(noeud->droite, element)) {
			noeud->equilibre--;
			if (noeud->equilibre == 0)
				return false;
			if (noeud->equilibre == -1)
				return true;
			assert(noeud->equilibre == -2);
			if(noeud->droite->equilibre == 1)
				rotationGaucheDroite(noeud->droite);
			rotationDroiteGauche(noeud);
		}
		return false;
	}

	// element == noeud->contenu
	noeud->contenu = element;  // Mise à jour
	return false;
}

template <class T>
void ArbreAVL<T>::rotationGaucheDroite(Noeud*& racinesousarbre) {
	Noeud *temp = racinesousarbre->gauche;
	int  ea = temp->equilibre;
	int  eb = racinesousarbre->equilibre;
	int  neb = eb - 1 - std::max(ea, 0);
	int  nea = ea -1 + std::min(neb, 0);

	temp->equilibre = nea;
	racinesousarbre->equilibre = neb;
	racinesousarbre->gauche = temp->droite;
	temp->droite = racinesousarbre;
	racinesousarbre = temp;
}

template <class T>
void ArbreAVL<T>::rotationDroiteGauche(Noeud*& racinesousarbre) {
	Noeud *temp = racinesousarbre->droite;
	// Référence https://cs.stackexchange.com/a/67818
	int  ea = temp->equilibre;
	int  eb = racinesousarbre->equilibre;
	int  neb = eb + 1 - std::min(ea, 0);
	int  nea = ea + 1 + std::max(neb, 0);

	temp->equilibre = nea;
	racinesousarbre->equilibre = neb;
	racinesousarbre->droite = temp->gauche;
	temp->gauche = racinesousarbre;
	racinesousarbre = temp;
}

template <class T>
bool ArbreAVL<T>::vide() const {
	return racine == NULL;
}

template <class T>
void ArbreAVL<T>::vider() {
	vider(racine);
	racine = NULL;
}

template <class T>
void ArbreAVL<T>::vider(Noeud*& noeud) {
	if (noeud != NULL) {
		vider(noeud->gauche);
		vider(noeud->droite);
		delete noeud;
	}
}

template <class T>
void ArbreAVL<T>::copier(const Noeud* source, Noeud*& noeud) const {
	if (source != NULL) {
		noeud = new Noeud(source->contenu);
		noeud->equilibre = source->equilibre;
		copier(source->gauche, noeud->gauche);
		copier(source->droite, noeud->droite);
	}
}

template <class T>
int  ArbreAVL<T>::hauteur() {
	return hauteur(racine);
}

template <class T>
int  ArbreAVL<T>::hauteur(Noeud*& noeud) const {
	if (noeud == NULL) {
		return 0;
	} else {
		return std::max(hauteur(noeud->gauche), hauteur(noeud->droite)) + 1;
	}
}

template <class T>
const T& ArbreAVL<T>::max(Noeud* n) const {
	assert(racine != NULL && "Impossible d'accéder aux valeurs d'un arbre vide.");
	while (n->droite != NULL) {
		n = n->droite;
	}
	return n->contenu;
}

// Ne fait rien si l'élément n'est pas dans l'arbre
template <class T>
void ArbreAVL<T>::enlever(const T& element) {
	enlever(racine, element);
}

// Retourne vrai si hauteur a changé à cause de l'enlevement
template <class T>
bool ArbreAVL<T>::enlever(Noeud*& noeud, const T& element) {
	if(element < noeud->contenu) {
		if(enlever(noeud->gauche, element)) {
			noeud->equilibre--;
			if (noeud->equilibre == 0) {
				return true;
			} else if (noeud->equilibre == -1) {
				return false;
			}
			assert(noeud->equilibre == -2);
			if(noeud->droite->equilibre == 1)
				rotationGaucheDroite(noeud->droite);
			rotationDroiteGauche(noeud);
			if (noeud->equilibre == 0) {
				return true;
			}
		}
		return false;
	}
	else if(element > noeud->contenu) {
		if(enlever(noeud->droite, element)) {
			noeud->equilibre++;
			if (noeud->equilibre == 0) {
				return true;
			} else if (noeud->equilibre == 1) {
				return false;
			}
			assert(noeud->equilibre==2);
			if(noeud->gauche->equilibre == -1)
				rotationDroiteGauche(noeud->gauche);
			rotationGaucheDroite(noeud);
			if (noeud->equilibre == 0) {
				return true;
			}
		}
		return false;
	}
	else if(element == noeud->contenu) {
		if (noeud->gauche==NULL && noeud->droite==NULL) {
			delete noeud;
			noeud = NULL;
		} else if (noeud->gauche==NULL || noeud->droite==NULL) {
			Noeud* temp = noeud->gauche != NULL ? noeud->gauche : noeud->droite;
			delete noeud;
			noeud = temp;
		} else {
			Noeud* temp = noeud->gauche;
			while (temp->droite != NULL) {
				temp = temp->droite;
			}
			T temp_contenu = temp->contenu;
			Noeud* actuel = noeud;
			enlever(temp->contenu);
			actuel->contenu = temp_contenu;
			return false;
		}
		return true;
	} else {
		return false;
	}
	return false;
}

//  fct retournant un Itérateur d'ArbreAVL
template <class T>
typename ArbreAVL<T>::Iterateur ArbreAVL<T>::debut() const {
	Noeud* noeud = racine;
	Iterateur iter(*this, noeud);
	if (noeud != NULL) {
		while(noeud->gauche != NULL) {
			iter.chemin.empiler(noeud);
			noeud = noeud->gauche;
		}
		iter.courant = noeud;
	}
	return iter;
}

template <class T>
typename ArbreAVL<T>::Iterateur ArbreAVL<T>::fin() const {
	return Iterateur(*this);
}

template <class T>
typename ArbreAVL<T>::Iterateur ArbreAVL<T>::rechercher(const T& e) const {
	Iterateur iter(*this, racine);
	iter.rechercher(e);
	return iter;
}

template <class T>
typename ArbreAVL<T>::Iterateur ArbreAVL<T>::rechercherEgalOuSuivant(const T& e) const {
	Iterateur iter(*this, racine);
	iter.rechercherEgalOuSuivant(e);
	return iter;
}

template <class T>
typename ArbreAVL<T>::Iterateur ArbreAVL<T>::rechercherEgalOuPrecedent(const T& e) const {
	Iterateur iter(*this, racine);
	iter.rechercherEgalOuPrecedent(e);
	return iter;
}

template <class T>
const T& ArbreAVL<T>::operator[](const Iterateur& iterateur) const {
	assert(&iterateur.arbre_associe == this);
	assert(iterateur.courant);
	return iterateur.courant->contenu;
}

template <class T>
T& ArbreAVL<T>::operator[](const Iterateur& iterateur) {
	assert(&iterateur.arbre_associe == this);
	assert(iterateur.courant);
	return iterateur.courant->contenu;
}

template <class T>
ArbreAVL<T>& ArbreAVL<T>::operator=(const ArbreAVL& autre) {
	if(this==&autre) return *this;
	vider();
	copier(autre.racine, racine);
	return *this;
}

//-----------------------
// Iterateur d'ArbreAVL
template <class T>
ArbreAVL<T>::Iterateur::Iterateur(const ArbreAVL& a)
 : arbre_associe(a), courant(NULL), chemin() {
}

template <class T>
ArbreAVL<T>::Iterateur::Iterateur(const ArbreAVL<T>::Iterateur& a)
: arbre_associe(a.arbre_associe) {
	courant = a.courant;
	chemin = a.chemin;
}

template <class T>
ArbreAVL<T>::Iterateur::Iterateur(const ArbreAVL& a, Noeud* c)
 : arbre_associe(a), courant(c), chemin() {
}

// Pré-incrément
template <class T>
typename ArbreAVL<T>::Iterateur& ArbreAVL<T>::Iterateur::operator++() {
	if (courant->droite != NULL) {
		courant = courant->droite;
		while (courant->gauche != NULL) {
			chemin.empiler(courant);
			courant = courant->gauche;
		}
	} else {
		if (!chemin.vide()) {
			courant = chemin.depiler();
		} else {
			courant = NULL;
		}
	}
	return *this;
}

// Post-incrément
template <class T>
typename ArbreAVL<T>::Iterateur ArbreAVL<T>::Iterateur::operator++(int) {
	Iterateur copie(*this);
	operator++();
	return copie;
}

template <class T>
ArbreAVL<T>::Iterateur::operator bool() const {
	return courant!=NULL;
}

template <class T>
bool ArbreAVL<T>::Iterateur::operator!() const{
	return courant==NULL;
}

template <class T>
bool ArbreAVL<T>::Iterateur::operator==(const Iterateur& o) const {
	assert(&arbre_associe==&o.arbre_associe);
	return courant==o.courant;
}

template <class T>
bool ArbreAVL<T>::Iterateur::operator!=(const Iterateur& o) const{
	assert(&arbre_associe==&o.arbre_associe);
	return courant!=o.courant;
}

template <class T>
T& ArbreAVL<T>::Iterateur::operator *() const {
	assert(courant!=NULL);
	return courant->contenu;
}

template <class T>
typename ArbreAVL<T>::Iterateur& ArbreAVL<T>::Iterateur::operator = (const Iterateur& autre){
	assert(&arbre_associe==&autre.arbre_associe);
	courant = autre.courant;
	chemin = autre.chemin;
	return *this;
}

//Il faudrait tenter de factoriser les parties communes dans une autre fonction
template <class T>
void ArbreAVL<T>::Iterateur::rechercher(const T& e) {
	while (courant != NULL && courant->contenu != e) {
		if (e < courant->contenu) {
			chemin.empiler(courant);
			courant = courant->gauche;
		} else {
			courant = courant->droite;
		}
	}
}

template <class T>
void ArbreAVL<T>::Iterateur::rechercherEgalOuSuivant(const T& e) {
	while (courant != NULL && courant->contenu != e) {
		if (e < courant->contenu) {
			chemin.empiler(courant);
			courant = courant->gauche;
		} else {
			courant = courant->droite;
		}
	}
	if (courant == NULL && !chemin.vide()) {
		courant = chemin.depiler();
	}
}

template <class T>
void ArbreAVL<T>::Iterateur::rechercherEgalOuPrecedent(const T& e) {
	Noeud* precedent = NULL;
	while (courant != NULL && courant->contenu != e) {
		if (e < courant->contenu) {
			chemin.empiler(courant);
			courant = courant->gauche;
		} else {
			precedent = courant;
			courant = courant->droite;
		}
	}
	if (courant == NULL || courant->contenu != e) {
		courant = precedent;
	}
}

#endif
