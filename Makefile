# Makefile pour TP2.
# Adaptez ce fichier au besoin.

# Options standard.
#OPTIONS = -Wall

# L'option -g permet de générer les infos de débogage.
# Décommentez la ligne suivante si vous désirez utiliser un débogueur.
#OPTIONS = -g3 -ggdb -O0 -Wall

# Les options -O, -O1, -O2, -O3 permetent d'optimiser le code binaire produit.
# Décommentez la ligne suivante avant la remise finale
OPTIONS = -O2 -Wall

all: tp2 testdate testavl test_arbremap
	$(MAKE) -C tests $(MAKECMDGOALS)

tp2: tp2.cpp inventaire.o date.o
	g++ $(OPTIONS) -o tp2 tp2.cpp inventaire.o date.o

date.o: date.h date.cpp
	g++ $(OPTIONS) -c -o date.o date.cpp

inventaire.o: inventaire.h inventaire.cpp arbreavl.h pile.h
	g++ $(OPTIONS) -c -o inventaire.o inventaire.cpp

testdate : testdate.cpp date.o
	g++ $(OPTIONS) -o testdate testdate.cpp date.o

testavl: testavl.cpp arbreavl.h
	g++ $(OPTIONS) -o testavl testavl.cpp

test_arbremap: test_arbremap.cpp arbremap.h
	g++ $(OPTIONS) -o test_arbremap test_arbremap.cpp

check: testdate testavl test_arbremap
	./testdate
	./testavl
	./test_arbremap
	$(MAKE) -C tests $(MAKECMDGOALS)
	./tests/evaluer.sh
	mkdir -p results
	-mv `ls test_[A-Z][0-9][0-9]+.txt` rapport-*.txt log-*.txt results

clean :
	rm -f *.o
	rm -f *.gch
	rm -f tp2
	rm -f testdate
	rm -f *~
	rm -f testavl
	rm -f test_arbremap
	rm -rf results
	rm -f `ls test_[A-Z][0-9][0-9]+.txt` rapport-*.txt log-*.txt
	$(MAKE) -C tests $(MAKECMDGOALS)
