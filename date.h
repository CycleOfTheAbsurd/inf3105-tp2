/*
  INF3105 - Structures de données et algorithmes
  UQAM / Département d'informatique
  Automne 2019

  Squelette de départ. Vous pouvez modifier ce fichier à votre guise.
  Vous n'êtes pas oubligés de l'utiliser.
*/

#if !defined(_DATE__H_)
#define _DATE__H_

#include <iostream>

class Date{
  public:
	Date();
	Date(const int jour, const int heure, const int minute);

	bool operator <(const Date& date) const;
	bool operator <=(const Date& date) const;
	bool operator ==(const Date& date) const;
	bool operator !=(const Date& date) const;

  private:
	int jour;
	int heure;
	int minute;

  friend std::ostream& operator << (std::ostream&, const Date& date);
  friend std::istream& operator >> (std::istream&, Date& date);
};

class Intervalle{
  private:
	Date debut;
	Date fin;
  public:
	Intervalle(){}

	// Il est parfois requis de briser l'abstraction
	Date getDebut() const { return debut; }
	Date getFin() const { return fin; }

	friend std::ostream& operator << (std::ostream&, const Intervalle&);
	friend std::istream& operator >> (std::istream&, Intervalle&);
};

#endif
